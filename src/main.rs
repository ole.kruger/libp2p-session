use libp2p::futures::StreamExt;
use libp2p::gossipsub;
use libp2p::gossipsub::IdentTopic;
use libp2p::identity::Keypair;
use libp2p::swarm::keep_alive;
use libp2p::swarm::NetworkBehaviour;
use libp2p::swarm::SwarmBuilder;
use libp2p::swarm::SwarmEvent;
use libp2p::tokio_development_transport;
use libp2p::Multiaddr;
use std::error::Error;

type R<T> = std::result::Result<T, Box<dyn Error>>;

#[derive(NetworkBehaviour)]
struct OurBehavior {
  keep_alive: keep_alive::Behaviour,
  gossipsub: gossipsub::Behaviour,
}

impl OurBehavior {
  fn new(key: Keypair) -> Self {
    Self {
      keep_alive: keep_alive::Behaviour,
      gossipsub: gossipsub::Behaviour::new(
        gossipsub::MessageAuthenticity::Signed(key),
        gossipsub::Config::default(),
      )
      .expect("Should work"),
    }
  }
}

#[tokio::main]
async fn main() -> R<()> {
  let keypair = Keypair::generate_ed25519();
  let peer_id = keypair.public().to_peer_id();

  let transport = tokio_development_transport(keypair.clone())?;

  let behavior = OurBehavior::new(keypair.clone());
  let mut swarm = SwarmBuilder::with_tokio_executor(transport, behavior, peer_id).build();

  let args = std::env::args().skip(1);
  if args.len() > 0 {
    for arg in args {
      swarm.dial(arg.parse::<Multiaddr>()?)?;
    }
  } else {
    swarm.listen_on("/ip4/0.0.0.0/tcp/0".parse()?)?;
  }

  swarm
    .behaviour_mut()
    .gossipsub
    .subscribe(&IdentTopic::new("chat"))?;

  loop {
    let event = swarm.next().await.unwrap();
    match &event {
      SwarmEvent::Behaviour(event) => match event {
        OurBehaviorEvent::Gossipsub(event) => {
          dbg!(event);
        }
        _ => {}
      },
      _ => {}
    };

    let result = swarm
      .behaviour_mut()
      .gossipsub
      .publish(IdentTopic::new("chat").hash(), vec![1, 2, 3, 4]);
    if result.is_err() {
      dbg!( & result);
    }
    dbg!(&event);
  }
}
